# Linux: Suckless-Patches

This is a collection of patches for suckless software. 

**Personal Builds:** Patches which are comprised of multiple public patches by other users, my own small addtitions and personal configurations such as font choice and color pallete. I use these patches on my main system and update them semi regularly.
**Custom Patches:** These patches are fully created by me. Small additions to functionality of looks mostly.

---

## Personal Builds:
![Arch PB Screenshot](https://i.ibb.co/fGm0ssh/DWM-FEB-2021.png)

These patches turn the individual suckless utilities into the fully extended and customized versions which I use on my machines. Currently my patches include the following utilities:
- dmenu
- dwm
- st
- slock
- slstatus
- tabbed

## Custom Patches
![Custom Screenshot](https://i.ibb.co/3fqMPzV/Sun-Jan-31-05-58-20-PM-UTC-2021.png)

I have created custom patches for the following utilities:

**sxiv:**

ColorCycle
- Set a default color scheme
- Cycle through additional color schemes with CTRL+C

FontCycle
- Setup array of fonts/sizes
- Cycle through font array with CTRL+F
